FROM python:3.5-alpine
RUN apk --update add git bash
RUN pip install git+https://github.com/dpallot/simple-websocket-server.git websocket-client
ENTRYPOINT ["python"]
CMD ["./testserver.py"]