import websocket
import json
import time

data = []
start = time.clock();
endpoint = "situation"

def on_message(ws, message):
    print(message)
    data.append(json.loads(message))
    if (time.clock()-start > 1):
        ws.close();

def on_error(ws, error):
    print(error)
    ws.close()

def on_close(ws):
    print(time.clock()-start)
    f = open(endpoint + ".json", "w")
    json.dump(data, f)
    f.close()
    print("### closed ###")

def on_open(ws):
    print("Websocket oppened")

if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp("ws://192.168.10.1/" + endpoint,
                                on_message = on_message,
                                on_error = on_error,
                                on_close = on_close)
    ws.on_open = on_open

    ws.run_forever()